//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////

module Game {
    export enum State {
        PLAYING,
        ENDED,
    };
}

class Main extends eui.UILayer {
    /**
     * 加载进度界面
     * loading process interface
     */
    private loadingView: LoadingUI;
    protected createChildren(): void {
        super.createChildren();
        //inject the custom material parser
        //注入自定义的素材解析器
        let assetAdapter = new AssetAdapter();
        egret.registerImplementation("eui.IAssetAdapter",assetAdapter);
        egret.registerImplementation("eui.IThemeAdapter",new ThemeAdapter());
        //Config loading process interface
        //设置加载进度界面
        this.loadingView = new LoadingUI();
        this.stage.addChild(this.loadingView);
        // initialize the Resource loading library
        //初始化Resource资源加载库
        RES.addEventListener(RES.ResourceEvent.CONFIG_COMPLETE, this.onConfigComplete, this);
        RES.loadConfig("resource/default.res.json", "resource/");
    }
    /**
     * 配置文件加载完成,开始预加载皮肤主题资源和preload资源组。
     * Loading of configuration file is complete, start to pre-load the theme configuration file and the preload resource group
     */
    private onConfigComplete(event:RES.ResourceEvent):void {
        RES.removeEventListener(RES.ResourceEvent.CONFIG_COMPLETE, this.onConfigComplete, this);
        // load skin theme configuration file, you can manually modify the file. And replace the default skin.
        //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
        let theme = new eui.Theme("resource/default.thm.json", this.stage);
        theme.addEventListener(eui.UIEvent.COMPLETE, this.onThemeLoadComplete, this);

        RES.addEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
        RES.addEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onResourceLoadError, this);
        RES.addEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onResourceProgress, this);
        RES.addEventListener(RES.ResourceEvent.ITEM_LOAD_ERROR, this.onItemLoadError, this);
        RES.loadGroup("preload");
    }
    private isThemeLoadEnd: boolean = false;
    /**
     * 主题文件加载完成,开始预加载
     * Loading of theme configuration file is complete, start to pre-load the 
     */
    private onThemeLoadComplete(): void {
        this.isThemeLoadEnd = true;
        this.createScene();
    }
    private isResourceLoadEnd: boolean = false;
    /**
     * preload资源组加载完成
     * preload resource group is loaded
     */
    private onResourceLoadComplete(event:RES.ResourceEvent):void {
        if (event.groupName == "preload") {
            this.stage.removeChild(this.loadingView);
            RES.removeEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
            RES.removeEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onResourceLoadError, this);
            RES.removeEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onResourceProgress, this);
            RES.removeEventListener(RES.ResourceEvent.ITEM_LOAD_ERROR, this.onItemLoadError, this);
            this.isResourceLoadEnd = true;
            this.createScene();
        }
    }
    private createScene(){
        if(this.isThemeLoadEnd && this.isResourceLoadEnd){
            this.startCreateScene();
        }
    }
    /**
     * 资源组加载出错
     *  The resource group loading failed
     */
    private onItemLoadError(event:RES.ResourceEvent):void {
        console.warn("Url:" + event.resItem.url + " has failed to load");
    }
    /**
     * 资源组加载出错
     * Resource group loading failed
     */
    private onResourceLoadError(event:RES.ResourceEvent):void {
        //TODO
        console.warn("Group:" + event.groupName + " has failed to load");
        //忽略加载失败的项目
        //ignore loading failed projects
        this.onResourceLoadComplete(event);
    }
    /**
     * preload资源组加载进度
     * loading process of preload resource
     */
    private onResourceProgress(event:RES.ResourceEvent):void {
        if (event.groupName == "preload") {
            this.loadingView.setProgress(event.itemsLoaded, event.itemsTotal);
        }
    }

    private groundArray: Array<egret.DisplayObject>;
    private skyArray: Array<egret.DisplayObject>;
    private pipes: Array<egret.DisplayObject>;
    private bird: egret.MovieClip;
    private minXGround:number;
    private minXSky: number;
    private minXPipe: number;
    private skyMoveXSpeed: number;
    private gameMoveXSpeed: number;
    private gameTimer: egret.Timer;
    private bgContainer: egret.DisplayObjectContainer;
    private objContainer: egret.DisplayObjectContainer;
    private gameState: Game.State;

    /**
     * 创建场景界面
     * Create scene interface
     */

    protected startCreateScene(): void {
        //add touch listener
        this.stage.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouchEvent, this);

        this.removeChildren();
        this.resetGameUI();
        this.configGame();
        this.startGame();
    }

    protected onTouchEvent(e:egret.TouchEvent):void {
        console.log("tap");

        if(this.gameState == Game.State.ENDED){
            this.removeChildren();
            this.resetGameUI();
            this.configGame();
            this.startGame();
            return;
        }

        egret.Tween.removeTweens(this.bird);
        egret.Tween.get(this.bird)
        .to({
            y:this.bird.y - 100,
            rotation: -50
        }, 500, egret.Ease.sineOut)
        // .to({
        //     rotation:0,
        // }, 20)
        .to({
            y:this.bird.y + 1000,
            rotation: 80
        }, 1000, egret.Ease.sineIn);
    }

    protected resetGameUI(){
        this.bgContainer = new egret.DisplayObjectContainer();
        this.addChildAt(this.bgContainer, 0);

        this.objContainer = new egret.DisplayObjectContainer();
        this.addChildAt(this.objContainer, 1);
        
        let sky = this.createSky();
        let ground = this.createGround();

        this.groundArray = [ground];
        this.skyArray = [sky];

        this.minXSky = sky.x + sky.width;
        this.minXGround = ground.x + ground.width;
        this.minXPipe = this.stage.stageWidth;

        this.bird = this.createBird();
    }

    protected configGame(){
        //setup game logic
        this.skyMoveXSpeed = -1000;
        this.gameMoveXSpeed = -7000;

        delete this.pipes;
        this.pipes = new Array<egret.DisplayObject>();
    }

    protected startGame(){
        console.log('start game');

        //remove all old action
        egret.Tween.removeAllTweens();

        //start game update timer
        delete this.gameTimer;
        this.gameTimer = new egret.Timer(0, 0);
        this.gameTimer.addEventListener(egret.TimerEvent.TIMER, this.mainUpdate, this);
        this.gameTimer.start();

        this.gameState = Game.State.PLAYING;
    }

    protected endGame() {
        console.log('end game');

        //stop timer
        this.gameTimer.stop();
        delete this.gameTimer;
        this.gameTimer = null;

        egret.Tween.removeAllTweens();

        this.bird.stop();

        this.gameState = Game.State.ENDED;

        //shake
        egret.Tween.get(this, {
            loop: true
        }).
        to({
            x:10,
            y:5,
        }, 50)
        .to({
            x:-10,
            y:-5,
        }, 100).
        to({
            x:0,
            y:0
        }, 50)
    }

    protected createBird(): egret.MovieClip {
        let texture = RES.getRes("birdfly_png");
        let data = RES.getRes("birdfly_json");

        var mcDataFactory = new egret.MovieClipDataFactory(data, texture);
        //创建 MovieClip，将工厂生成的 MovieClipData 传入参数
        var mc = new egret.MovieClip(mcDataFactory.generateMovieClipData("bird"));
        mc.gotoAndPlay('fly', -1);
        this.addChildAt(mc, 3);

        mc.x = this.stage.stageWidth*0.5;
        mc.y = this.stage.stageHeight*0.5;

        mc.scaleX = mc.scaleY = 2;

        return mc;
    }

    protected createSky(): egret.Bitmap {
        let sky = this.createBitmapByName("sky_png");
        this.bgContainer.addChild(sky);
        sky.width = this.stage.stageWidth;
        sky.height = this.stage.stageHeight*0.7;
        sky.y = this.stage.stageHeight*0.3;
        return sky;
    }

    protected createGround(): egret.Bitmap {
        let ground = this.createBitmapByName("land_png");
        this.objContainer.addChild(ground);
        ground.width = this.stage.stageWidth;
        ground.height = this.stage.stageHeight*0.5;
        ground.y = this.stage.stageHeight*0.8;
        return ground;
    }

    protected createPipe(): Array<egret.Bitmap> {
        let pipeTop = this.createBitmapByName("PipeDown_png");
        let pipeBot = this.createBitmapByName("PipeUp_png");

        this.objContainer.addChild(pipeTop);
        this.objContainer.addChild(pipeBot);

        pipeBot.width = pipeTop.width = this.stage.stageWidth*0.2;
        pipeBot.height = pipeTop.height = this.stage.stageHeight;

        pipeBot.y = pipeTop.y = this.stage.stageHeight*0.5;
        pipeBot.x = pipeTop.x = this.stage.stageWidth*0.5;

        let randomSpace = Math.random()*100 + 200;
        let randomOffsetY = Math.random()*150;

        pipeTop.anchorOffsetY = pipeBot.height + randomSpace*0.5 + randomOffsetY;
        pipeBot.anchorOffsetY = -randomSpace*0.5 + randomOffsetY;

        let arr = new Array<egret.Bitmap>();
        arr.push(pipeTop);
        arr.push(pipeBot);
        return arr;
    }

    protected mainUpdate(e:egret.TimerEvent): void {
        let delta = (<egret.Timer> e.currentTarget).delay;
        
        this.skyUpdate(delta);
        this.gameUpdate(delta);

        e.updateAfterEvent();
    }

    protected skyUpdate(delta:number):void {
        //movement system
        let skyMoveOffsetX = delta*this.skyMoveXSpeed/1000;
        for(let sky of this.skyArray){
            sky.x += skyMoveOffsetX;
        }
        this.minXSky += skyMoveOffsetX;

        //remove
        if(this.skyArray.length != 0){
            let firstSky = this.skyArray[0];
            if(firstSky.x + firstSky.width < 0){
                firstSky.x = this.minXSky;
                this.minXSky += firstSky.width;
                this.skyArray.shift();
                this.skyArray.push(firstSky);
            }
        }

        //generation system
        if(this.minXSky < this.stage.stageWidth){
            //create new sky
            let sky = this.createSky();
            this.skyArray.push(sky);
            sky.x = this.minXSky;
            this.minXSky += sky.width;
        }
    }

     protected gameUpdate(delta: number):void {
        //movement system
        let gameMoveOffsetX = delta*this.gameMoveXSpeed/1000;
        // for(let ground of this.groundArray){
        //     ground.x += gameMoveOffsetX;
        // }
        // this.minXGround += gameMoveOffsetX;

        // //remove
        // if(this.groundArray.length != 0){
        //     let firstGround = this.groundArray[0];
        //     if(firstGround.x + firstGround.width < 0){
        //         firstGround.x = this.minXGround;
        //         this.minXGround = firstGround.x + firstGround.width;
        //         this.groundArray.shift();
        //         this.groundArray.push(firstGround);
        //     }
        // }

        // //generation system
        // if(this.minXGround < this.stage.stageWidth){
        //     //create new ground
        //     let ground = this.createGround();
        //     this.groundArray.push(ground);
        //     ground.x = this.minXGround;
        //     this.minXGround += ground.width;
        // }

        //pipe movement
        for(let pipe of this.pipes){
            pipe.x = pipe.x + gameMoveOffsetX;
        }
        this.minXPipe += gameMoveOffsetX;

        //pipe remove
        if( this.pipes.length != 0){
            let firstPipe = this.pipes[0];
            if(firstPipe.x + firstPipe.width < 0){
                firstPipe.parent.removeChild(firstPipe);
                this.pipes.shift();
            }
        }

        //pipe generate
        if(this.minXPipe < this.stage.stageWidth*1.5){
            //create pipe
            let newPipes = this.createPipe();
            this.pipes.push(newPipes[0], newPipes[1]);
            let randomX = Math.random()*100 + 220;
            newPipes[0].x = newPipes[1].x = this.minXPipe + randomX;
            this.minXPipe = newPipes[0].x + newPipes[0].width;
        }

        //collision with land

        let birdBox: egret.Rectangle;
        birdBox = this.bird.getTransformedBounds(this.stage, birdBox);
        console.log("rect = " + birdBox);

        if(birdBox.bottom >= this.groundArray[0].y
        || this.bird.y < 0)
        {
            console.log("hit land => die");
            this.endGame();
            return;
        }

        let pipeBox: egret.Rectangle;
        //collision with pipe
        for(let pipe of this.pipes){
            pipeBox = pipe.getTransformedBounds(this.stage, pipeBox);
            if(pipeBox.intersects(birdBox)){
                console.log("hit pipe => die");
                this.endGame();
                return;
            }
        }
    }

    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    private createBitmapByName(name:string):egret.Bitmap {
        let result = new egret.Bitmap();
        let texture:egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }
   
    /**
     * 点击按钮
     * Click the button
     */
    private onButtonClick(e: egret.TouchEvent) {
        let panel = new eui.Panel();
        panel.title = "Title";
        panel.horizontalCenter = 0;
        panel.verticalCenter = 0;
        this.addChild(panel);
    }
}
